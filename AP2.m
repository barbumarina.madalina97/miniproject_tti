function AP2(p)
Cbsc = 1 + p.*log2(p) + (1-p).*log2(1-p);
Crsc = log2(3) + p.*log2(p/2) + (1-p).*log2(1-p);
Crsc(p==0) = log2(3);
Crsc(p==1) = log2(3) - 1;
Cbsc(or(p==0, p==1)) = 1;
Crsc
plot(p,Cbsc, 'r-',p,Crsc,'k:');
xlabel('p');
ylabel('Cbsc/Crsc');
legend('Cbsc', 'Crsc');
title('Capacitatea canalului binar simetric si a canalului ternar simetric');