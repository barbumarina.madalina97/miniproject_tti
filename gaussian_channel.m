function C = gaussian_channel ( )
% put e r ea s emnalului de l a i n t r a r e
P_X = 1 ;
% put e r ea zgomotului
N = linspace ( 0.1,2,1000) ;
% banda de f r e c v e n t a
W = linspace ( 1,2,5 ) ;
% i n i t i a l i z a r e c a p a c i t a t e
C = zeros (length(N), length(W)) ;
for i = 1:length(N)
for j = 1:length(W)
C(i,j) = W(j)*log2(1+P_X/N(i)) ;
end
end
figure(),plot(N,C),
xlabel('N[Watt]') , ylabel('C[biti/secunda]') ,
title('Capacitatea canalului gaussian'),
grid(),
legend('W=1Hz','W=1.25Hz','W=1.5Hz' ,'W=1.75Hz' , 'W=2Hz')
end