function AP1(p)
Cbsc = 1 + p.*log2(p) + (1-p).*log2(1-p);
Cbec = 1 - p;
Cbsc(or(p==0, p==1)) = 1;
Cbsc
plot(p,Cbsc, 'r-',p,Cbec,'k:');
xlabel('p');
ylabel('Cbsc/Cbec');
legend('Cbsc', 'Cbec');
title('Capacitatea canalului binar simetric si a canalului cu anulari');