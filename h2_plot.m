function h2_plot(p)
h2 = -p.*log2(p) - (1-p).*log2(1-p);
h2(or(p==0,p==1))=0;
h2
figure(), plot(p,h2), xlabel('p'), ylabel('h2(p)');
end

