function AP3(a)
I = - ((a/2).*log2(a/2)  + (1-(a/2)).*log2(1-(a/2)) + a);
I(a==0) = 0;
plot(a,I);
xlabel('a');
ylabel('I');
grid;
end
