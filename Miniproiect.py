import numpy as np
from math import log2
import matplotlib.pyplot as plt

def h2_plot():
    p = np.linspace(0,1,100)
    h2 = []
    for i in range(0,100):
        if p[i] == 0 or p[i] == 1:
            h2.append(0)
        else:
            h2.append(-p[i] * log2(p[i]) - (1 - p[i]) * log2(1 - p[i]))

    plt.plot(p,h2)
    plt.grid()
    plt.xlabel('p')
    plt.ylabel('h2')
    plt.title('Graficul h2(p)')
    plt.show()


def gaussian_channel():
    P_X = 1
    N = np.linspace(0.1,2,1000)
    W = np.linspace(1,2,5)
    C = np.zeros((N.size, W.size))

    for i in range(N.size):
        for j in range(W.size):
            C[i,j] = W[j]*log2(1+P_X/N[i])

    plt.plot(N, C[:,0], label ='W=1Hz')
    plt.plot(N, C[:,1], label='W=1.25Hz')
    plt.plot(N, C[:,2], label='W=1.5Hz')
    plt.plot(N, C[:,3], label='W=1.75Hz')
    plt.plot(N, C[:,4], label='W=2Hz')

    plt.xlabel('N[Watt]')
    plt.ylabel('C[biti/secunda]')
    plt.title('Capacitate canalului gaussian')
    plt.grid()
    plt.legend()
    plt.show()
    return C


def ap1():
    p = np.linspace(0,1,100)
    Cbsc = []
    Cbec = []
    for i in range(100):
        if p[i] == 0 or p[i] == 1:
            Cbsc.append(1)
            Cbec.append(1 - p[i])
        else:
            Cbsc.append(1 + p[i]*log2(p[i]) + (1 - p[i])*log2(1 - p[i]))
            Cbec.append(1 - p[i])

    plt.plot(p, Cbsc, label = 'Canal Binar Simetric')
    plt.plot(p, Cbec, label = 'Cabal binar cu anulari')
    plt.grid()
    plt.xlabel('p')
    plt.ylabel('Cbsc/Cbec')
    plt.title('Capacitatea canalului binar simetric si a canalului cu anulari')
    plt.legend()
    plt.show()


def ap2():
    p = np.linspace(0,1,100)
    Cbsc = []
    Ctsc = []
    for i in range(100):
        if p[i] == 0:
            Cbsc.append(1)
            Ctsc.append(log2(3))
        elif p[i] == 1:
            Cbsc.append(1)
            Ctsc.append(log2(3) - 1)
        else:
            Cbsc.append(1 + p[i] * log2(p[i]) + (1 - p[i]) * log2(1 - p[i]))
            Ctsc.append(log2(3) + p[i] * log2(p[i]/2) + (1-p[i])*log2(1-p[i]))

    plt.plot(p, Cbsc, label='Canal Binar Simetric')
    plt.plot(p, Ctsc, label='Cabal ternar simetric')
    plt.grid()
    plt.xlabel('p')
    plt.ylabel('Cbsc/Ctsc')
    plt.title('Capacitatea canalului binar simetric si a canalului ternar simetric')
    plt.legend()
    plt.show()


def ap3():
    a = np.linspace(0,1,100)
    I = []
    for i in range(100):
        if a[i] == 0:
            I.append(0)
        else:
            I.append(-((a[i]/2) *log2(a[i]/2) + (1- a[i]/2)*log2(1- a[i]/2) + a[i] ))

    plt.plot(a, I)
    plt.xlabel('a')
    plt.ylabel('I')
    plt.grid()
    plt.title('Informatia mutuala pentru canalul Z')
    plt.show()


h2_plot()
gaussian_channel()
ap1()
ap2()
ap3()